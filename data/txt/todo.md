
## todo C64
* expansion port    
    * MMC64, MMC Replay    
    * SuperCPU
    * Magic Voice
    * Dongles
    * clockport emulation (i.e. Retro Replay, MMC)
    * CMD Ramlink + HD
    * all the others
* disk
    * ram extension
    * Virtual Device Traps
    * drive sounds
    * support command line start of disk listings instead of Load "*"
    * mount OS folder as D64
    * non standard floppy models: 1571, 1581
* user port
    * RS232
    * SpeedDOS / DolphinDOS
    * 4 player adapter
    * all the others
* tape   
    * content listing in user interface, like disks
    * click entries and load them without fast-forward to counter position before
    * Dongles
* 16/24k Kernal support
* PAL color banding (alternating red/green bars)
* SID file player

## todo Amiga
* A500
* slow/fast memory expansion
* IPF disk image format
* Action Replay
* 68020 Turbo Card
* Hard Disk
* A1200


## todo for all emulated systems
* 100 Hz black frame insertion
* support for directx 11, vulkan, metal graphics driver
* SPIR-V, RetroArch Shader support
* Debug monitor for developers
* autofire with frequency and option to fire without button press
* support each setting as command line option
* Beam Racing
* rewind support
* screenshots/movie recording
* screenshots for savestates
* 7z support
* Netplay
