
#include "drive1541.h"

namespace LIBC64 {   
    
auto Drive1541::rotateD64() -> void {    
    
    if (!motorRun())
        return;

    // the images were created in standard cbm dos format.
    // for cbm dos we know the amount of bits passed the
    // read/write head every second according to the speedzone.
    
    // 1.000.000 cpu cycles = bps ( bits per second for current speedzone )
    // 1 cpu cycle          = x bits
    // x bits for one cpu cycle = bps / 1.000.000 drive cpu cycles
    
    accum += rotSpeedBps[speedZone];
    
    if (accum < driveCycles)
        return;
    // one bit has moved
    accum -= driveCycles;
    
    uint8_t byte;
    uint8_t* trackPtr = gcrTrack->data;
    
    if (readMode) {
                
        if ( !loaded || !trackPtr ) 
            // no image loaded or track not present

            byte = 0;
        else
            // headOffset is the bit position within a track.            
            // we move the actual bit to the most significant bit.
            byte = trackPtr[ headOffset >> 3 ] << (headOffset & 7);
            
        headOffset++;
        // move next bit to msb.
        byte <<= 1;

        if  ( !( headOffset & 7 ) ) {

            if ( (headOffset >> 3) >= gcrTrack->size) {
                // revolution complete ... wrap around
                headOffset = 0;
            }
            // fetch next byte
            byte = (!loaded || !trackPtr) ? 0 : trackPtr[ headOffset >> 3 ];
        }

        // make room for incomming bit
        readBuffer <<= 1;
        writeBuffer <<= 1;
        // append incomming bit
        readBuffer |= (byte >> 7) & 1;
        readBuffer &= 0x3ff; // 10 bit buffer

        if ((readBuffer & 0xf) == 0)
            // when there are more than three zeros in a row, a one will be injected by drive mechanic
            readBuffer |= 1;

        // if last 10 bits in a row are non zero then there is a sync.
        // in this case data is not moving.
        if (~readBuffer & 0x3ff) {
            // no sync 
            if (++ue3Counter == 8)
                byteFetched( false );
            else
                via2->ca1In( true );

        } else
            ue3Counter = 0; //reset when sync mark detected
        
    } else {
        // because of shared bus
        readBuffer = (readBuffer << 1) & 0x3fe;

        if ((readBuffer & 0xf) == 0)
            readBuffer |= 1;

        writeBit( (writeBuffer & 0x80) != 0 );
        writeBuffer <<= 1;

        if (++ue3Counter == 8) {
            ue3Counter = 0;

            writeBuffer = writeValue; // fetch next byte to buffer

            if ( byteReadyOverflow )
                cpu->triggerSO();

            via2->ca1In( !byteReadyOverflow );
        } else {

            via2->ca1In( true );
        }
    }
}

auto Drive1541::byteFetched( bool overflowNotThisCycle ) -> void {
    // later than 0.75 of first half cycle miss CPU detection of external overflow or interrupt in this cycle

    ue3Counter = 0;
    latchedByte = writeBuffer = readBuffer & 0xff;

    if (byteReadyOverflow)
        // edge transition
        cpu->triggerSO(overflowNotThisCycle ? 2 : 1);

    // edge transition, but direction matters so we emulate the PIN state and not only the transiton like external overflow
    via2->ca1In(!byteReadyOverflow, overflowNotThisCycle);
}

inline auto Drive1541::readBit() -> bool {
    uint8_t* trackPtr = gcrTrack->data;
    
    if (!loaded)
        return 0;

    unsigned byte = headOffset >> 3;
    uint8_t bit = (~headOffset) & 7; // msb is next
    
    headOffset++;

    if ( headOffset >= gcrTrack->bits )
        headOffset = 0; // wrap around the ring buffer 
    
    if (!trackPtr)
        return 0;
    
    return (trackPtr[byte] >> bit) & 1;
}

inline auto Drive1541::writeBit( bool state ) -> void {
    uint8_t* trackPtr = gcrTrack->data;
    
    if (!loaded)
        return;
    
    unsigned byte = headOffset >> 3;
    uint8_t bit = (~headOffset) & 7; // msb is next
    
    headOffset++;

    if ( headOffset >= gcrTrack->bits )
        headOffset = 0; // wrap around the ring buffer 
    
    if (!trackPtr || writeProtected)
        return;
        
    if (state)
        trackPtr[byte] |= 1 << bit;
    else
        trackPtr[byte] &= ~(1 << bit);    
    
    if (!written)
        written = true;
    
    gcrTrack->written = 1; // track data has changed, host have to write back
}

auto Drive1541::motorRun() -> bool {

    if (motorOn)
        return true;

    if (!motorOff.slowDown)
        return false;

    if (motorOff.delay) {
        motorOff.delay--;
        return true;
    }

    // Star Trekking game needs emulation of motor slow down
    unsigned decelerationPoint = motorOff.decelerationPoint;
    if (motorOff.chunkSize[decelerationPoint])
        motorOff.chunkSize[decelerationPoint]--;

    if (motorOff.chunkSize[decelerationPoint] == 0) {
        if (motorOff.decelerationPoint)
            motorOff.decelerationPoint--;
        else {
            motorOff.slowDown = false;
            return false;
        }
    }

    if (motorOff.pos++ <= decelerationPoint)
        return true;

    if (motorOff.pos == (motorOff.CHUNKS + 1) )
        motorOff.pos = 0;

    return false;
}

auto Drive1541::motorOffInit() -> void {

    motorOff.delay = 14000 + (rand() % 1000);
    unsigned slowDownCycles = 50000;

    unsigned chunkSize = slowDownCycles / motorOff.CHUNKS;
    unsigned rest = slowDownCycles % motorOff.CHUNKS;

    for(unsigned i = 0; i < motorOff.CHUNKS; i++)
        motorOff.chunkSize[i] = chunkSize;

    for(unsigned i = 0; i < rest; i++)
        motorOff.chunkSize[i % motorOff.CHUNKS]++;

    motorOff.decelerationPoint = motorOff.CHUNKS - 1;
    motorOff.pos = 0;
    motorOff.slowDown = true;
}

auto Drive1541::randomizeRpm() -> void {
    
    // drive speed is 300 rounds per minute
    // more realistic speed wobbles between 299,75 - 300,25
    // so we could generate a random number in a range of 0.5
    // generating random integer numbers is easier, lets scale up
    // 0.5 rpm * 100 = 50
    // 300 rpm * 100 = 30000
    unsigned adjusted = rpm + (rand() % (wobble + 1) ) - (wobble / 2);
    // there are fixed values how many bits passed the r/w head each second within a speed zone.
    // however these values are valid for a rotation speed of exactly 300 rpm
    // we solve this by a simple proportion:
    // when
    // adjusted = 1000000 cpu cycles
    // then
    // 30000 = drive cycles per second
    // drive cycles per second = 30000 * 1000000 / adjusted
    driveCycles = (30000ULL * 1000000ULL) / adjusted;    
    // so we get the amount of cycles per second for adjusted motor speed.
    // now we could calculate the amount of bits passed for any amount of cpu drive cycles
    // by following proportion:
    // bits per speedzone [bps] = drive cycles per second
    // bits passed              = cpu cycles passed
    
    // bits passed = bits per speedzone * cpu cycles passed / drive cycles per second
    
    // for g64 rotation, we apply the randomness for drive speed on reference cycles
    refCyclesPerRevolution = (30000ULL * CyclesPerRevolution300Rpm) / adjusted;
}

// 1 - 0 = 1
// 2 - 1 = 1
// 3 - 2 = 1
// 0 - 3 = 1
// 1 - 0 = 1
// 2 - 1 = 1
// 1 - 2 = (-1) 3
// 0 - 1 = (-1) 3
// 3 - 0 = 3
// 2 - 3 = (-1) 3

auto Drive1541::updateStepper( uint8_t step ) -> bool {
    
    if (step == 1) {        
        if (currentHalftrack < ((MAX_TRACKS_1541 * 2) - 1) ) {
            currentHalftrack++;
            stepDirection = 1;
            return true;            
        }
            
        stepDirection = -1;
        
    } else if (step == 3) {
        
        if (currentHalftrack > 0) {
            currentHalftrack--;
            stepDirection = -1;
            return true;
        }
            
        stepDirection = 1;
        
    } else if (step == 2) {
        // Primitive 7 Sins uses this method
        if (stepDirection == 1) {            
            if (currentHalftrack & 1) {
                if (updateStepper(1))
                    return updateStepper(1);
            }
            
        } else if (stepDirection == -1) {
            if ((currentHalftrack & 1) == 0) {
                if (updateStepper(3))
                    return updateStepper(3);
            }
        } 
    }
    
    return false;
}

auto Drive1541::changeHalfTrack( uint8_t step ) -> void {
                    
    updateStepper( step );

    if (structure1541.type == Structure1541::Type::P64) {

        unsigned position = 0;

        if (pulseIndex >= 0) {
            Structure1541::Pulse& pulse = gcrTrack->pulses[pulseIndex];

            if (pulse.position > pulseDelta)
                position = pulse.position - pulseDelta;
            else
                position = CyclesPerRevolution300Rpm - (pulseDelta - pulse.position);
        }

        gcrTrack = structure1541.getTrackPtr( currentHalftrack );
        pulseIndex = gcrTrack->firstPulse;
        pulseDelta = 1;

        while ((pulseIndex >= 0) && (gcrTrack->pulses[pulseIndex].position <= position))
            pulseIndex = gcrTrack->pulses[pulseIndex].next;

        if (pulseIndex >= 0)
            pulseDelta = gcrTrack->pulses[pulseIndex].position - position;
        else {
            pulseIndex = gcrTrack->firstPulse;
            if (pulseIndex >= 0)
                pulseDelta = (CyclesPerRevolution300Rpm - position) + gcrTrack->pulses[pulseIndex].position;
        }

    } else {    // D64, G64
        unsigned oldTrackSize = gcrTrack->size;

        // pointer to next track
        gcrTrack = structure1541.getTrackPtr( currentHalftrack );

        if ( oldTrackSize != 0 )
            // we want to keep alignment between old and new track.
            // head offset doesn't change if both tracks have same size, otherwise we use a simple proportion
            // old head offset = new head offset
            // old size = new size
            // new head offset = old head * new size / old size
            // i heard of games which rely on correct alignment of track data.
            headOffset = ( headOffset * gcrTrack->size ) / oldTrackSize;

         else
            headOffset = 0;
    }

    updateDeviceState( );

}

inline auto Drive1541::syncFound() -> uint8_t {
    
    if (!readMode || attachDelay )
        return 0x80;
    
    return readBuffer == 0x3ff ? 0 : 0x80;
}

}

